# enseirb-s9-ia

Projet d'IA de Go

# Authors

The maintainers of this project are :

- Andricque Maëlle (mandricque@enseirb-matmeca.fr)
- Canet Ivan (icanet@enseirb-matmeca.fr)

## Strategies

We have implemented minimax with alpha-beta pruning.
We forbid our player to pass (unless it's the only option left).

Our heuristic measures the number of stones each player has. It penalizes stones in the diagonals of the board (to discourage building big blocks that can be easily taken).

We select the depth of recursion based on an estimation of the moves that need to be calculated, and the time left (more time = more depth, less moves = more depth).
This way, we can adapt calculate a low depth when many moves are available, and keep our time budget for when difficult moves need to be played. Hopefully, this should leave us with more time left when the opponent has expanded theirs.
