# -*- coding: utf-8 -*-
""" This is the file you have to modify for the tournament. Your default AI player must be called
by this module, in the myPlayer class.

Right now, this class contains the copy of the randomPlayer. But you have to change this!
"""
import time
from typing import List

import Goban
from playerInterface import *

LOGGER_SIZE = 0
max_score = 1000

BLACK = 0
WHITE = 0
EMPTY = 0

MAX_TIME = 8 * 60


class myPlayer(PlayerInterface):
    """ Example of a random player for the go. The only tricky part is to be able to handle
    the internal representation of moves given by legal_moves() and used by push() and
    to translate them to the GO-move strings "A1", ..., "J8", "PASS". Easy!

    """

    # noinspection PyProtectedMember
    def __init__(self):
        self._board = Goban.Board()
        self._mycolor = None
        self._opponent = None
        self._opponentPassed = False

        self._timeLeft = MAX_TIME

        global BLACK, WHITE, EMPTY
        BLACK = self._board._BLACK
        WHITE = self._board._WHITE
        EMPTY = self._board._EMPTY

    def getPlayerName(self):
        return "Maëlle & Ivan"

    def calculateScore(self) -> int:
        score = self._board.diff_stones_board()

        return score if self._mycolor == BLACK else -score

    # noinspection PyMethodMayBeStatic
    def scorePenaltyFor(self, move: int) -> float:
        diagonal_penalty = 0.5 if (move % 2 == 0) else 0
        return diagonal_penalty

    def selectNextMove(self, allied_player=True, depth=2, alpha=-max_score, beta=max_score) -> Tuple[int, float]:
        global LOGGER_SIZE
        moves: List[int] = self._board.legal_moves()[:-1]

        score = -max_score if allied_player else max_score
        best_move: int = -1

        for move in moves:
            self._board.push(move)
            LOGGER_SIZE += 1

            (nextBestMove, nextBestScore) = best_move, score

            if depth <= 1:
                (nextBestMove, nextBestScore) = move, \
                                                self.calculateScore() - self.scorePenaltyFor(move)
                # print(LOGGER_SIZE * " ", Goban.Board.flat_to_name(move), nextBestScore)
            elif not self._board.is_game_over():
                # print(LOGGER_SIZE * " ", Goban.Board.flat_to_name(move))
                (nextBestMove, nextBestScore) = self.selectNextMove(not allied_player, depth - 1, alpha, beta)
                nextBestScore -= self.scorePenaltyFor(move)

            if allied_player and nextBestScore > score:
                (best_move, score) = move, nextBestScore
            elif not allied_player and nextBestScore < score:
                (best_move, score) = move, nextBestScore

            if allied_player:
                alpha = max(alpha, score)
            else:
                beta = min(beta, score)

            LOGGER_SIZE -= 1
            self._board.pop()

            if alpha >= beta:
                break

        # print(LOGGER_SIZE * " ", "Minimax:", Goban.Board.flat_to_name(best_move), score)
        return best_move, score

    def getPlayerMove(self):
        if self._board.is_game_over():
            print("Referee told me to play but the game is over!")
            return "PASS"
        print()
        currentScore = self.calculateScore()
        print("Current score:\t", currentScore)
        if currentScore > 0 and self._opponentPassed:
            print("The opponent passed, and I have a better score. I'm passing as well.")
            return "PASS"

        start = time.perf_counter()

        moves = len(self._board.weak_legal_moves())

        preferredDepth = int(60 / moves - (60 / moves ** 2)) + 2
        maximalDepth = 6
        if self._timeLeft < 0.05 * MAX_TIME:
            maximalDepth = 1
        elif self._timeLeft < 0.10 * MAX_TIME:
            maximalDepth = 2
        elif self._timeLeft < 0.20 * MAX_TIME:
            maximalDepth = 3
        elif self._timeLeft < 0.50 * MAX_TIME:
            maximalDepth = 4
        depth = min(maximalDepth, preferredDepth)
        print("Allowed depth:\t", maximalDepth, "(because of", int(self._timeLeft), "s left)")
        print("Preferred depth:\t", preferredDepth, "(because of", moves, "moves available)")
        print("Selected depth:\t", depth)
        (move, score) = self.selectNextMove(depth=depth)
        print("Selected move:\t", Goban.Board.flat_to_name(move), "\tscore:", score)
        self._board.push(move)

        end = time.perf_counter()
        self._timeLeft -= end - start
        return Goban.Board.flat_to_name(move)

    def playOpponentMove(self, move: str):
        print("Opponent played ", move)  # New here

        flat_move = Goban.Board.name_to_flat(move)
        self._opponentPassed = flat_move == -1

        # the board needs an internal represetation to push the move.  Not a string
        self._board.push(flat_move)

    def newGame(self, color):
        self._mycolor = color
        self._opponent = Goban.Board.flip(color)

    def endGame(self, winner):
        if self._mycolor == winner:
            print("I won!!!")
        else:
            print("I lost :(!!")
